package com.example.tp1_springboot.service;

import com.example.tp1_springboot.beans.User;
import com.example.tp1_springboot.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public int  save(User user) {
        if (user == null) return -1;
        else {
            userDao.save(user);
            return 1;

        }
    }
    public List<User> findAll() {
        return userDao.findAll();
    }

    public User findById(Long id){
        return userDao.findById(id).get();
    }
    public void deleteById(Long id){
         userDao.deleteById(id);
    }
}
