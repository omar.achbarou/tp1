package com.example.tp1_springboot.ws;

import com.example.tp1_springboot.beans.User;
import com.example.tp1_springboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/users")
public class UserProvided {

    @Autowired
    UserService service;

    @GetMapping("/")
    public List<User> findAll() {
        return service.findAll();
    }
    @PostMapping("/")
    public int save(@RequestBody User user){
        System.out.println("User impl  "+user.getLastName());
        return service.save(user);
    }
    @GetMapping("/id/{id}")
    public User findById(@PathVariable Long id){
        return service.findById(id);
    }
    @DeleteMapping("/id/{id}")
    public void deleteById(@PathVariable Long id){
         service.deleteById(id);
    }
    @PutMapping("/id/{id}")
    public void UpdateById(@PathVariable Long id, @RequestBody User user){
        service.deleteById(id);
        service.save(user);
    }
}
